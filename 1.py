import pandas as pd
import numpy as np
import math
from scipy.stats import chi2
import matplotlib.pyplot as plt


def sum_2(arr) :
    ret_sum = arr[0]**2
    for i in range(1, len(arr), 1):
        ret_sum += arr[i] ** 2
    return ret_sum


def fi(value) :
    return 1 / np.sqrt(2 * np.pi) * np.e ** (-((value**2) / 2))


def F(value) :
    return 1 / 2 * (math.erf(value / np.sqrt(2)) + 1)


def ks2_B(N, bins, wk, p_):
    ks2_B = 0
    for i in range(len(bins) - 1):
        ks2_B += (N * (wk[i] - p_[i])**2) / p_[i]

    critical_value = chi2.ppf(1 - 0.05, len(bins) - 1 - 1)  # Вычисление критического значения хи-квадрат
    print("\nВычисление выборочного значения критерия Ks2[B]:")
    print("Ks2[B] =", ks2_B)
    print("Критическое значение =", critical_value)

    if ks2_B < critical_value:
        print("Результат: Гипотеза о соответствии выборки нормальному распределению не отвергается.")
    else:
        print("Результат: Гипотеза о соответствии выборки нормальному распределению отвергается.")


def main() :
    N = 200
    NAME_FILE = '1.csv'

    data = np.genfromtxt(NAME_FILE, delimiter=';')

    arr = data.ravel()

    reshaped_arr = arr.reshape((20, 10))
    print("Неупорядоченная выборка (10x20):")
    print(reshaped_arr)

    print("Упорядоченная выборка (10x20):")

    arr = np.sort(arr)  # сортировка
    sorted_arr = arr.reshape((20, 10))
    print(sorted_arr)

    a0 = min(arr)
    am = max(arr)
    
    m = 1 + int(np.log2(N))

    print("a[0] = {}, a[{}] = {}".format(a0, m, am))

    bins = np.linspace(a0, am, m + 1)

    counts, _ = np.histogram(arr, bins=bins)
    
    wk = counts / N
    nk = counts

    print("Интервал | nk | wk")
    for i in range(len(bins) - 1):
        if ( i == 0 ) :
            print(f"[{bins[i]:.5f}, {bins[i + 1]:.5f}], {nk[i]}, {wk[i]:.5f}")
        else :
            print(f"({bins[i]:.5f}, {bins[i + 1]:.5f}], {nk[i]}, {wk[i]:.5f}")

    print("-", sum(nk), sum(wk))



    grade_m_a = 1/N * sum(arr)
    grade_s = 1/N * sum_2(arr) - grade_m_a
    grade_d = np.sqrt(grade_s)
    print("grade_m_a = {:.5f} \ngrade_s = {:.5f} \ngrade_d = {:.5f}".format(
                grade_m_a, 
                grade_s, 
                grade_d))

    tmp = 1 / grade_d 

    
    print("k  |   ak   | (ak - m_a) / grade_d |  f()  |  F()  | p*[k]")
    p_ = []
    for i in range(len(bins)):
        b = (bins[i] - grade_m_a) / grade_d
        c = 1 / grade_d * fi(b)
        f = F(b)
        p_k = 0
        if ( i == 0 ) :
            print("{}, {:.5f}, {:.5f}, {:.5f}, {:.5f}, -".format(i, bins[i], b, c, f))
            continue
        elif ( i == 1 ) :
            p_k = f
        elif ( i == len(bins) - 1 ) :
            p_k = 1 - F((bins[i - 1] - grade_m_a) / grade_d)
        else :
            p_k = f - F((bins[i - 1] - grade_m_a) / grade_d)

        print("{}, {:.5f}, {:.5f}, {:.5f}, {:.5f}, {:.5f}".format(i, bins[i], b, c, f, p_k))
        p_.append(p_k)
    print("sum(p*[k] = {:.5f}".format(sum(p_)))
    

    plt.figure(figsize=(12, 6))

    # Гистограмма относительных частот
    plt.hist(arr, bins=bins, density=True, edgecolor='black', alpha=0.7, label='Гистограмма относительных частот')

    # График плотности нормального распределения
    x = np.linspace(a0, am, 100)
    y = 1 / (grade_d * np.sqrt(2 * np.pi)) * np.exp(-(x - grade_m_a)**2 / (2 * grade_d**2))
    plt.plot(x, y, color='red', label='Плотность нормального распределения N(^a, ^sigma^2)')

    plt.xlabel('Значение')
    plt.ylabel('Плотность вероятности / Относительная частота')
    plt.title('Гистограмма и плотность нормального распределения')
    plt.legend()

    plt.show()

    print("Вычисление выборочного значения критерия Ks2[B]")
    print("k  | интервал |  w[k] |  p*[k]  |  abs(w[k] - p*[k]) | ks() / p*k")
    arr_max_p = []
    sum_N = []
    for i in range(len(bins) - 1):
        arr_max_p.append(abs(wk[i] - p_[i]))
        sum_N.append((N * (wk[i] - p_[i])) / p_[i])
        if ( i == 0 ) :
            print(f"{i + 1} [{bins[i]:.5f}, {bins[i + 1]:.5f}], {wk[i]:.5f}, \
{p_[i]:.5f}, {arr_max_p[i]:.5f}, {sum_N[i]:.5f}")
        else :
            print(f"{i + 1} ({bins[i]:.5f}, {bins[i + 1]:.5f}], {wk[i]:.5f}, \
{p_[i]:.5f}, {arr_max_p[i]:.5f}, {sum_N[i]:.5f}")

    print("-, -, {:.5f}, {:.5f}, {:.5f}, {:.5f}".format(sum(wk), sum(p_), max(arr_max_p), sum(sum_N)))

    ks2_B(N, bins, wk, p_)

    return 0
    


if __name__ == "__main__":
    exit(main()) 
